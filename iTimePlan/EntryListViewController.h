//
//  MainViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/18/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MZTimerLabel.h"
#import "DataCenter.h"
#import "EntryCell.h"

#define kMinSaveInterval    60          // 最小60秒才能被保存

@interface EntryListViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, EntryCellDelegate, UIGestureRecognizerDelegate>
{
    Entry *confirmEntry;
    Record *currentRecord;      // current record's reference
    
    NSTimer *scheduleTimer;     //  更新cells用的
    
    UITapGestureRecognizer *tapReg;
}

@property (nonatomic, strong) NSArray *entries;
@property (nonatomic, strong) Plan *plan;           // plan reference
@property (nonatomic, strong) NSArray *titlesArr;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)newEntryAction:(id)sender;
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;

- (void)notificationEntriesUpdated:(NSNotification *)notify;

@end
