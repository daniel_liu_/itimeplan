//
//  Utility.h
//  iTimePlan
//
//  Created by LiuHuanMing on 5/5/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject

+ (NSString *)stringByInterval:(float)intv short:(BOOL)flag;
+ (NSString *)stringByInterval:(float)intv;

@end
