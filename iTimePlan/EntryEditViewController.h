//
//  EntryEditViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/29/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"

#import "RMDateSelectionViewController.h"

#import "RMImageScroller.h"
#import "RMUIUtils.h"

#import "DataCenter.h"

@interface EntryEditViewController : UIViewController <RMImageScrollerDelegate, UITextViewDelegate>

@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *titleField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *remarkField;

@property (weak, nonatomic) IBOutlet UILabel *reminderLabel;
@property (strong, nonatomic) RMDateSelectionViewController *datePickerVC;
@property (weak, nonatomic) IBOutlet UISwitch *dateSwitch;

@property (strong, nonatomic) Plan *plan;
@property (strong, nonatomic) Entry *entry;

@property (strong, nonatomic) NSString *tmpIconName;
@property (nonatomic, assign) BOOL enableCancel;        // 如果为enableCancel则一直显示

- (void)cancelAction:(id)sender;
- (void)saveAction:(id)sender;
- (IBAction)iconClicked:(id)sender;
- (IBAction)reminderAction:(UISwitch *)sender;

- (void)checkTitleEmpty;
- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture;

- (void)textFieldTextDidChanged:(NSNotification *)notify;
- (void)notificationEntriesUpdated:(NSNotification *)notify;

@end
