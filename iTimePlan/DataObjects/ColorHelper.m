//
//  ColorHelper.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/29/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "ColorHelper.h"

#import "UIColor+HexColors.h"
#import "CJSONDeserializer.h"

static NSMutableArray *colors = nil;
static int currentIndex = 0;

@implementation ColorHelper

+ (void)load
{
    //  init colors config
    NSString *path = [[NSBundle mainBundle] pathForResource:@"colors" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSAssert1(data, @"Not found Colors at path: %@", path);
    colors = [[CJSONDeserializer deserializer] deserializeAsArray:data error:nil];
}

+ (UIColor *)randomColor
{
    currentIndex++;
    if (currentIndex >= colors.count-1)
        currentIndex = 0;
    
    NSArray *subArray = [colors objectAtIndex:currentIndex];
    NSString *hexStr = [subArray objectAtIndex:arc4random()%subArray.count];
    return [UIColor colorWithHexString:hexStr];
}

+ (UIColor *)randomColor:(int)index
{
    NSArray *subArray = [colors objectAtIndex:index%(colors.count)];
    NSString *hexStr = [subArray objectAtIndex:arc4random()%subArray.count];
    return [UIColor colorWithHexString:hexStr];
}

@end
