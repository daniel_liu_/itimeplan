//
//  EntityCell.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/19/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Entry.h"
#import "Record.h"

#import "MCSwipeTableViewCell.h"

@class EntryCell;

@protocol EntryCellDelegate <NSObject>
- (void)entryCellFinishedDidClicked:(EntryCell *)entryCell;
@end

@interface EntryCell : MCSwipeTableViewCell
{
    NSDateFormatter *fmt;
    int playStopFlag;           /* 0=play, 1=stop */
}

@property (strong, nonatomic) Entry *entry;
@property (assign, nonatomic) id<EntryCellDelegate> eDelegate;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *clockIcon;
@property (weak, nonatomic) IBOutlet UILabel *secondsLabel;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;

- (void)updateEntry:(Entry *)entry_;
- (void)updateCountDown:(Record *)rcd;

- (void)finishDidClicked:(id)sender;

@end
