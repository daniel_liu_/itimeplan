//
//  ColorHelper.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/29/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorHelper : NSObject

+ (UIColor *)randomColor;
+ (UIColor *)randomColor:(int)index;

@end
