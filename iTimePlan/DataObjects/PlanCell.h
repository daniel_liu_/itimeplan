//
//  PlanCell.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/28/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TDBadgedCell.h"

@interface PlanCell : TDBadgedCell

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;

@end
