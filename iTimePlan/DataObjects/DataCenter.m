//
//  DataCenter.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/19/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "DataCenter.h"

@implementation DataCenter

static DataCenter *sharedDataCenterInstance = nil;

- (id)init
{
    self = [super init];
    if (self) {
        // 0. init plans
        self.plans = [[NSMutableArray alloc] init];
        NSArray *plansArray = [DBHelper getAllPlans];
        for (NSDictionary *pln in plansArray)
            [self.plans addObject:[self planFromDictionary:pln]];
        
        // 1. init entries
        self.entries = [[NSMutableArray alloc] init];
        NSArray *entriesArray = [DBHelper getAllEntries];
        for (NSDictionary *ent in entriesArray)
            [self.entries addObject:[self entryFromDictionary:ent]];
        
        //  2. init records
        self.records = [[NSMutableArray alloc] init];
        NSArray *recordsArray = [DBHelper getAllRecords];
        for (NSDictionary *dict in recordsArray)
            [self.records addObject:[self recordFromDictionary:dict]];
    }
    return self;
}

+ (DataCenter *)sharedDataCenter
{
    @synchronized(self) {
        if (sharedDataCenterInstance == nil) {
            sharedDataCenterInstance = [[self alloc] init];
        }
        return sharedDataCenterInstance;
    }
}

#pragma mark - Utility

- (Record *)recordFromDictionary:(NSDictionary *)dict
{
    if (!dict) return nil;
    
    Record *r = [ObjectConverter createObject:[Record class] fromDictionary:dict];
    r.entry = [self getEntryById:[[dict objectForKey:@"entryid"] intValue]];
    return r;
}

- (NSDictionary *)dictionaryFromRecord:(Record *)record
{
    if (!record) return nil;
    
    NSMutableDictionary *dict = [ObjectConverter createDictionaryFromObject:record];
    [dict setObject:[NSNumber numberWithInt:record.entry.id] forKey:@"entryid"];
    [dict setObject:[NSNumber numberWithInt:record.entry.plan.id] forKey:@"planid"];        // 用来删除临时record用的
    return dict;
}

- (Entry *)entryFromDictionary:(NSDictionary *)dict
{
    if (!dict) return nil;
    
    Entry *e = [ObjectConverter createObject:[Entry class] fromDictionary:dict];
    e.plan = [self getPlanById:[[dict objectForKey:@"planid"] intValue]];
    return e;
}

- (NSDictionary *)dictionaryFromEntry:(Entry *)entry
{
    if (!entry) return nil;
    
    NSMutableDictionary *dict = [ObjectConverter createDictionaryFromObject:entry];
    if (entry.remark == nil) [dict setObject:@"" forKey:@"remark"];
    if (entry.plan) [dict setObject:[NSNumber numberWithInt:entry.plan.id] forKey:@"planid"];
    else [dict setObject:[NSNumber numberWithInt:kPlanTableTempId] forKey:@"planid"];      // 默认临时列表的id
    return dict;
}

- (Plan *)planFromDictionary:(NSDictionary *)dict
{
    if (!dict) return nil;
    
    Plan *p = [ObjectConverter createObject:[Plan class] fromDictionary:dict];
    return p;
}

- (NSDictionary *)dictionaryFromPlan:(Plan *)plan
{
    if (!plan) return nil;
    
    NSMutableDictionary *dict = [ObjectConverter createDictionaryFromObject:plan];
    if (plan.remark == nil) [dict setObject:@"" forKey:@"remark"];
    return dict;
}

#pragma mark - Record

- (void)newTmpRecord:(Record *)record planId:(int)planId
{
    if (record) {
        NSMutableDictionary *dict = (NSMutableDictionary *)[self dictionaryFromRecord:record];
        [dict setObject:[NSNumber numberWithInt:planId] forKey:@"planid"];
        [DBHelper newTmpRecord:dict];
    }
}

- (Record *)getTmpRecord:(int)planId
{
    NSDictionary *dict = [DBHelper getTmpRecord:planId];
    if (dict) return [self recordFromDictionary:dict];
    return nil;
}

- (void)newRecord:(Record *)record
{
    if (record) {
        int nid = [DBHelper newRecord:[self dictionaryFromRecord:record]];
        record.id = nid;
        [self.records addObject:record];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRecordsUpdated object:record];
    }
}

- (void)updateRecord:(Record *)record
{
    if (record) {
        //  update database
        [DBHelper updateRecord:[self dictionaryFromRecord:record]];
        
        //  update records array
        Record *obj = [self getRecordById:record.id];
        if (obj) [self.records replaceObjectAtIndex:[self.records indexOfObject:obj] withObject:record];
        
        //  notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRecordsUpdated object:nil];
    }
}

- (Record *)getRecordById:(int)rid
{
    for (Record *robj in self.records) {
        if (robj.id == rid) return robj;
    }
    return nil;
}

- (NSArray *)getRecordsByDate:(NSDate *)day planId:(int)planId
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    NSArray *arr = [DBHelper getRecordsByDate:day planId:planId];
    for (NSDictionary *dict in arr)
        [tmpArray addObject:[self recordFromDictionary:dict]];
    return tmpArray;
}

- (NSArray *)getRecordsByDate:(NSDate *)day
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    NSArray *arr = [DBHelper getRecordsByDate:day];
    for (NSDictionary *dict in arr)
        [tmpArray addObject:[self recordFromDictionary:dict]];
    return tmpArray;
}

- (NSArray *)getRecordsByDay:(NSDate *)day
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    NSArray *arr = [DBHelper getRecordsByDay:day];
    for (NSDictionary *dict in arr)
        [tmpArray addObject:[self recordFromDictionary:dict]];
    return tmpArray;
}

- (NSDictionary *)getRecordsTitleDictByDate:(NSDate *)day
{
    return [self getRecordsTitleDictByDate:day planId:kPlanTableTempId];
}

/*
 dictionary key is 'entity name' such as '看电影'
 value is an array inclued all records by the same entity
 */
- (NSDictionary *)getRecordsTitleDictByDate:(NSDate *)day planId:(int)planId
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *recordsArray = [self getRecordsByDate:day planId:planId];
    
    //  1. get the keys
    NSMutableArray *titleArr = [[NSMutableArray alloc] init];   // entity titles
    for (Record *rec in recordsArray) {
        if ([titleArr containsObject:rec.entry.title] == NO) {
            [titleArr addObject:rec.entry.title];
        }
    }
    
    //  2. get array records by every key
    for (NSString *title_ in titleArr) {
        NSMutableArray *tArr = [[NSMutableArray alloc] init];
        for (Record *r_ in recordsArray) {
            if ([r_.entry.title isEqualToString:title_]) {
                [tArr addObject:r_];
            }
        }
        [dict setObject:tArr forKey:title_];
    }
    
    return dict;
}

- (NSDictionary *)getRecordsTitleDictByDay:(NSDate *)day
{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *recordsArray = [self getRecordsByDay:day];
    
    //  1. get the keys
    NSMutableArray *titleArr = [[NSMutableArray alloc] init];   // entity titles
    for (Record *rec in recordsArray) {
        if ([titleArr containsObject:rec.entry.title] == NO) {
            [titleArr addObject:rec.entry.title];
        }
    }
    
    //  2. get array records by every key
    for (NSString *title_ in titleArr) {
        NSMutableArray *tArr = [[NSMutableArray alloc] init];
        for (Record *r_ in recordsArray) {
            if ([r_.entry.title isEqualToString:title_]) {
                [tArr addObject:r_];
            }
        }
        [dict setObject:tArr forKey:title_];
    }
    
    return dict;
}

/*
    得到某天中某个entryid的时间
*/
- (NSTimeInterval)getSecondsByEntryId:(int)entryId withDate:(NSDate *)day
{
    NSTimeInterval t = 0.0f;
    NSArray *tmpArray = [self getRecordsByDate:day];
    for (Record *rcd in tmpArray)
        if (rcd.entry.id == entryId) t += rcd.interval;
    return t;
}

/*
    得到day与day-1这两个日期之间关于entryId的差值
 */
- (NSTimeInterval)getDeltaSec:(int)entryId beforeDate:(NSDate *)day
{
    NSTimeInterval t1 = [self getSecondsByEntryId:entryId withDate:day];
    NSTimeInterval t0 = [self getSecondsByEntryId:entryId withDate:[day dateByAddingTimeInterval:-(24*60*60)]]; // 前一天
    return t1 - t0;
}

#pragma mark - Entry

- (Entry *)getEntryById:(int)eid
{
    for (int i = 0; i < self.entries.count; i++) {
        Entry *e = [self.entries objectAtIndex:i];
        if (e.id == eid) return e;
    }
    return nil;
}

- (NSArray *)getEntriesByPlanId:(int)pid
{
    return [self getEntriesByPlanId:pid type:0];
}

// type: 0,1,2 - 全，未完成，完成
- (NSArray *)getEntriesByPlanId:(int)pid type:(int)type
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (Entry *e in self.entries) {
        if (type == 1) {
            if (e.plan.id == pid && e.finish == 0) [arr addObject:e];
        }else if (type == 2) {
            if (e.plan.id == pid && e.finish) [arr addObject:e];
        }else {
            if (e.plan.id == pid) [arr addObject:e];
        }
    }
    return arr;
}

- (void)newEntry:(Entry *)entry
{
    if (entry) {
        int nid = [DBHelper newEntry:[self dictionaryFromEntry:entry]];
        entry.id = nid;
        [self.entries addObject:entry];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEntriesUpdated object:entry];
    }
}

- (void)updateEntry:(Entry *)entry
{
    if (entry) {
        //  update database
        [DBHelper updateEntry:[self dictionaryFromEntry:entry]];
        
        //  update array
        Entry *obj = [self getEntryById:entry.id];
        if (obj) [self.entries replaceObjectAtIndex:[self.entries indexOfObject:obj] withObject:entry];
        
        //  notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEntriesUpdated object:nil];
    }
}

- (void)deleteEntry:(Entry *)entry
{
    if (entry) {
        //  update database
        [DBHelper deleteEntry:entry.id];
        
        //  update entries array
        [self.entries removeObject:entry];
        
        //  update records array
        [self.records removeAllObjects];
        NSArray *recordsArray = [DBHelper getAllRecords];
        for (NSDictionary *dict in recordsArray)
            [self.records addObject:[self recordFromDictionary:dict]];
        
        //  notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEntriesUpdated object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRecordsUpdated object:nil];
    }
}

- (void)destoryEntriesByPlanId:(int)pid
{
    //  check temp plan id
    if (pid != kPlanTableTempId) {
        BOOL succ = [DBHelper destoryEntriesByPlanId:pid];
        if (succ) {
            //  update entries array
            [self.entries removeAllObjects];
            NSArray *entriesArray = [DBHelper getAllEntries];
            for (NSDictionary *ent in entriesArray)
                [self.entries addObject:[self entryFromDictionary:ent]];
            
            //  update records array
            [self.records removeAllObjects];
            NSArray *recordsArray = [DBHelper getAllRecords];
            for (NSDictionary *dict in recordsArray)
                [self.records addObject:[self recordFromDictionary:dict]];
            
            //  notification
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEntriesUpdated object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRecordsUpdated object:nil];
        }
    }
}

- (NSTimeInterval)getEntrySeconds:(int)eid
{
    return [DBHelper getEntrySeconds:eid];
}

#pragma mark - Plan

- (Plan *)getPlanById:(int)pid
{
    for (int i = 0; i < self.plans.count; i++) {
        Plan *p = [self.plans objectAtIndex:i];
        if (p.id == pid) return p;
    }
    return nil;
}

- (NSArray *)getLastModifyPlans:(int)num
{
    NSMutableArray *tmpArr = [[NSMutableArray alloc] init];
    NSArray *arr = [DBHelper getLastModifyPlans:num];
    for (NSDictionary *dict in arr) {
        Plan *p = [self planFromDictionary:dict];
        [tmpArr addObject:p];
    }
    return tmpArr;
}

- (int)newPlan:(Plan *)plan
{
    int nid = 0;
    if (plan) {
        nid = [DBHelper newPlan:[self dictionaryFromPlan:plan]];
        plan.id = nid;
        [self.plans addObject:plan];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlansUpdated object:plan];
    }
    return nid;
}

- (void)updatePlan:(Plan *)plan
{
    if (plan) {
        //  update database
        [DBHelper updatePlan:[self dictionaryFromPlan:plan]];
        
        //  update plans array
        Plan *obj = [self getPlanById:plan.id];
        if (obj) [self.plans replaceObjectAtIndex:[self.plans indexOfObject:obj] withObject:plan];
        
        //  notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlansUpdated object:nil];
    }
}

- (void)deletePlan:(Plan *)plan
{
    if (plan) {
        //  update database
        [DBHelper deletePlan:plan.id];
        
        //  update plans array
        [self.plans removeObject:plan];
        
        //  notification
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPlansUpdated object:nil];
    }
}

// 获取未完成的badge数
- (int)getTodoBadgeInPlan:(int)pid
{
    return [DBHelper getTodoBadgeInPlan:pid];
}

#pragma mark - Import & Export

- (void)exportPlan:(int)planid toPath:(NSString *)path
{
    NSMutableDictionary *destDict = [[NSMutableDictionary alloc] init];
    
    NSDictionary *dict = [DBHelper getPlanById:planid];
    NSArray *arr = [DBHelper getEntriesByPlanId:planid];
    [destDict setObject:dict forKey:@"plan"];
    [destDict setObject:arr forKey:@"entry"];
    
    NSData *data = [[CJSONSerializer serializer] serializeObject:destDict error:nil];
    [data writeToFile:path atomically:YES];
}

- (void)importPlanFromPath:(NSString *)path
{
    NSData *data = [NSData dataWithContentsOfFile:path];
    NSDictionary *dict = [[CJSONDeserializer deserializer] deserialize:data error:nil];
    
    NSDictionary *plan = [dict objectForKey:@"plan"];
    NSArray *entry = [dict objectForKey:@"entry"];
    
    //  new plan
    int planid = [self newPlan:[self planFromDictionary:plan]];
    
    //  new entry in plan
    for (NSDictionary *d_ in entry) {
        NSMutableDictionary *nd_ = [NSMutableDictionary dictionaryWithDictionary:d_];
        [nd_ setObject:[NSNumber numberWithInt:planid] forKey:@"planid"];
        Entry *e_ = [self entryFromDictionary:nd_];
        [self newEntry:e_];
    }
}

@end

