//
//  PlanCell.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/28/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "PlanCell.h"

@implementation PlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
