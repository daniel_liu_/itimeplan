//
//  DataCenter.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/19/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DBHelper.h"
#import "Plan.h"
#import "Record.h"
#import "Entry.h"
#import "ObjectConverter.h"

#define kLastModifyPlanNum              3               // last 3 modified plans

//  Records, plans & entries
#define kNotificationRecordsUpdated     @"kNotificationRecordsUpdated"
#define kNotificationPlansUpdated       @"kNotificationPlansUpdated"
#define kNotificationEntriesUpdated     @"kNotificationEntriesUpdated"

@interface DataCenter : NSObject

@property(nonatomic, strong) NSMutableArray *plans;     // plan object
@property(nonatomic, strong) NSMutableArray *entries;   // entry object
@property(nonatomic, strong) NSMutableArray *records;   // record object

+ (DataCenter *)sharedDataCenter;

//  utility
- (Record *)recordFromDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryFromRecord:(Record *)record;

- (Entry *)entryFromDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryFromEntry:(Entry *)entry;

- (Plan *)planFromDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryFromPlan:(Plan *)plan;

//  temp record
- (void)newTmpRecord:(Record *)record planId:(int)planId;
- (Record *)getTmpRecord:(int)planId;

//  record
- (void)newRecord:(Record *)record;
- (void)updateRecord:(Record *)record;
- (Record *)getRecordById:(int)rid;

- (NSArray *)getRecordsByDate:(NSDate *)day planId:(int)planId;
- (NSArray *)getRecordsByDate:(NSDate *)day;
- (NSArray *)getRecordsByDay:(NSDate *)day;
- (NSDictionary *)getRecordsTitleDictByDate:(NSDate *)day;
- (NSDictionary *)getRecordsTitleDictByDate:(NSDate *)day planId:(int)planId;
- (NSDictionary *)getRecordsTitleDictByDay:(NSDate *)day;
- (NSTimeInterval)getSecondsByEntryId:(int)entryId withDate:(NSDate *)day;
- (NSTimeInterval)getDeltaSec:(int)entryId beforeDate:(NSDate *)day;

//  Entry
- (Entry *)getEntryById:(int)eid;
- (NSArray *)getEntriesByPlanId:(int)pid;
- (NSArray *)getEntriesByPlanId:(int)pid type:(int)type;    // type: 0,1,2 - 全，未完成，完成
- (void)newEntry:(Entry *)entry;
- (void)updateEntry:(Entry *)entry;
- (void)deleteEntry:(Entry *)entry;
- (void)destoryEntriesByPlanId:(int)pid;    // 毁灭性的，代表会删除相应的，有破坏力的，所以是destory
- (NSTimeInterval)getEntrySeconds:(int)eid;

//  Plan
- (Plan *)getPlanById:(int)pid;
- (NSArray *)getLastModifyPlans:(int)num;   // 获取最近更新过的plans
- (int)newPlan:(Plan *)plan;
- (void)updatePlan:(Plan *)plan;
- (void)deletePlan:(Plan *)plan;
- (int)getTodoBadgeInPlan:(int)pid;         // 获取未完成的badge数

//  Import & Export
- (void)exportPlan:(int)planid toPath:(NSString *)path;
- (void)importPlanFromPath:(NSString *)path;

@end

