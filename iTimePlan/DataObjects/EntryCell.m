//
//  EntityCell.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/19/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "EntryCell.h"

#import "DataCenter.h"

@implementation EntryCell

@synthesize entry;
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"HH:mm:ss"];
    [fmt setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    //  button action
    [self.finishButton addTarget:self action:@selector(finishDidClicked:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateFinishStatus:(BOOL)isFinished
{
    NSString *imgName = [NSString stringWithFormat:@"cell_finish_%d.png", isFinished];
    [self.finishButton setImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    
    NSString *imgName2 = [NSString stringWithFormat:@"cell_finish_%d.png", !isFinished];
    [self.finishButton setImage:[UIImage imageNamed:imgName2] forState:UIControlStateHighlighted];
}

- (void)updateCountDown:(Record *)rcd
{
    if (rcd == nil) {
        self.clockIcon.hidden = YES;
        self.secondsLabel.textColor = [UIColor grayColor];
    } else {
        self.clockIcon.hidden = !(rcd.entry.goaltime > 0);      // 有倒计时才显示
        
        NSTimeInterval delta = [NSDate timeIntervalSinceReferenceDate] - rcd.timestamp;
        if (delta < rcd.entry.goaltime) delta = rcd.entry.goaltime - delta;
        
        NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:delta];
        self.secondsLabel.text = [fmt stringFromDate:d];
        self.secondsLabel.textColor = [UIColor colorWithRed:96.0/255.0 green:112.0/255.0 blue:188.0/255.0 alpha:1.0];
    }
}

- (void)updateEntry:(Entry *)entry_
{
    self.entry = entry_;
    
    self.titleLabel.text = entry_.title;
    NSTimeInterval t = [[DataCenter sharedDataCenter] getEntrySeconds:self.entry.id];
    self.secondsLabel.text = [Utility stringByInterval:t];
    
    [self updateFinishStatus:entry_.finish];
}

- (void)finishDidClicked:(id)sender
{
    Entry *e = self.entry;
    e.finish = !e.finish;
    
    [[DataCenter sharedDataCenter] updateEntry:e];
    
    //  call delegate
    if (self.eDelegate && [self.eDelegate respondsToSelector:@selector(entryCellFinishedDidClicked:)]) {
        [self.eDelegate entryCellFinishedDidClicked:self];
    }
}

@end
