//
//  AppDelegate.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/18/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
