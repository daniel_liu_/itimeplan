//
//  HistoryViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/18/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "HistoryViewController.h"

#import "RecordCell.h"
#import "UINavigationController+Fade.h"
#import "PieChartViewController.h"

#define kTotalIconName  @"EA_0039.png"
#define kTotalTitle     @"当日总汇"

@implementation HistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  day picker
    self.dayPicker = [[MZDayPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 52) dayCellSize:CGSizeZero dayCellFooterHeight:0.0f];
    self.dayPicker.delegate = self;
    self.dayPicker.dataSource = self;
    self.dayPicker.dayNameLabelFontSize = 10.0f;
    self.dayPicker.dayLabelFontSize = 16.0f;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"EE"];
    [self.bannerView addSubview:self.dayPicker];
    
    //  nav bar
    self.plans = [[DataCenter sharedDataCenter] getLastModifyPlans:kLastModifyPlanNum];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:@[kTotalIconName, kTotalTitle]];
    for (Plan *p in self.plans) [items addObject:@[p.icon, p.title]];
    navBarDrawer = [[ACNavBarDrawer alloc] initWithView:self.view andItemInfoArray:items];
    navBarDrawer.delegate = self;
    
    //  UI
    currentPlan = nil;
    self.title = kTotalTitle;
    
    //  notification
    NSNotificationCenter *notifyCenter = [NSNotificationCenter defaultCenter];
    [notifyCenter addObserver:self selector:@selector(notificationRecordsUpdated:) name:kNotificationRecordsUpdated object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  update day picker every time when view did appear
    NSDate *day = [NSDate date];
    NSDate *last7 = [day dateByAddingTimeInterval:-7*(24*60*60)];   // 7天内
    [self.dayPicker setStartDate:last7 endDate:day];
    [self.dayPicker setCurrentDate:day animated:NO];
    
    //  new datas
    if (currentPlan == nil) {
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDay:self.dayPicker.currentDate];
    }else {
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDate:self.dayPicker.currentDate planId:currentPlan.id];
    }
    [self.tableView reloadData];
    
    //  title gesture
    tapReg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigationBarTapped:)];
    [self.navigationController.navigationBar addGestureRecognizer:tapReg];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar removeGestureRecognizer:tapReg];
}

#pragma mark - MZDayPickerDataSource

- (NSString *)dayPicker:(MZDayPicker *)dayPicker titleForCellDayNameLabelInDay:(MZDay *)day
{
    return [self.dateFormatter stringFromDate:day.date];
}

#pragma mark - MZDayPickerDelegate

- (void)dayPicker:(MZDayPicker *)dayPicker didSelectDay:(MZDay *)day
{
    if (currentPlan == nil) {
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDay:day.date];
    }else {
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDate:day.date planId:currentPlan.id];
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentRecord = [recordsArray objectAtIndex:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 54.0f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return recordsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"RecordCell";
    RecordCell * cell=  (RecordCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) cell = [[RecordCell alloc] init];
    
    Record *rcd = [recordsArray objectAtIndex:indexPath.row];
    [cell.iconView setImage:[UIImage imageNamed:rcd.entry.icon]];
    [cell.titleLabel setText:rcd.entry.title];
    [cell.subLabel setText:[rcd durationStr]];
    [cell.hoursLabel setText:[rcd hoursStr]];
    
    if (indexPath.row % 2 == 0) cell.backgroundColor = [UIColor colorWithRed:.96 green:.96 blue:.96 alpha:.9];
    
    return cell;
}

#pragma mark - ACNavBarDrawerDelegate

- (void)didTapButtonAtIndex:(NSInteger)itemIndex
{
    //  0 = 今日总汇
    if (itemIndex == 0) {
        self.title = kTotalTitle;
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDay:self.dayPicker.currentDate];
    }else {
        currentPlan = [self.plans objectAtIndex:itemIndex-1];   // minus 1
        
        self.title = currentPlan.title;
        recordsArray = [[DataCenter sharedDataCenter] getRecordsByDate:self.dayPicker.currentDate planId:currentPlan.id];
    }
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)chartAction:(id *)sender
{
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PieChartViewController *pcvc = [stb instantiateViewControllerWithIdentifier:@"PieChartViewController"];
    pcvc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewControllerFade:pcvc];
}

- (void)navigationBarTapped:(id)sender
{
    if (navBarDrawer.isOpen) [navBarDrawer closeNavBarDrawer];
    else [navBarDrawer openNavBarDrawer];
}

#pragma mark - Notifications

- (void)notificationRecordsUpdated:(NSNotification *)notify
{
    self.plans = [[DataCenter sharedDataCenter] getLastModifyPlans:kLastModifyPlanNum];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:@[kTotalIconName, kTotalTitle]];
    for (Plan *p in self.plans) [items addObject:@[p.icon, p.title]];
    [navBarDrawer updateWithArray:items];
}

@end

