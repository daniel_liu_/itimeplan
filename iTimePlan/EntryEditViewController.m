//
//  EntryEditViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/29/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "EntryEditViewController.h"

@implementation EntryEditViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"新任务";
    
    //  regist bar buttons
    [self registerBarButtons];
    
    //  init title Fields
    self.titleField.placeholder = @"标题";
    self.remarkField.placeholder = @"备注";
    self.remarkField.delegate = self;
    self.titleField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    //  init date formatter
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"HH:mm"];
    
    //  icon picker
    RMImageScroller *scroller = [[RMImageScroller alloc] initWithFrame:CGRectMake(0, 400, self.view.frame.size.width, 90)];
	scroller.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	scroller.delegate = self;
	scroller.padding = 6;
    scroller.imageWidth = 48;
	scroller.imageHeight = 48;
	scroller.hideSlider = YES;
	scroller.separatorWidth = 10;
	scroller.spreadFirstPageAlone = YES;
	scroller.spreadMode = NO;
    [scroller.selectedTilePrototype copyStyleOf:scroller.tilePrototype];
    [self.view addSubview:scroller];
    
    //  date picker
    self.datePickerVC = [RMDateSelectionViewController dateSelectionController];
    self.datePickerVC.hideNowButton = YES;
    
    //  temp icon name
    self.tmpIconName = @"EA_0001.png";
    [self.iconView setImage:[UIImage imageNamed:self.tmpIconName]];
    
    //  edit entry
    if (self.entry) {
        self.titleField.text = self.entry.title;
        self.remarkField.text = self.entry.remark;
        self.tmpIconName = self.entry.icon;
        self.iconView.image = [UIImage imageNamed:self.entry.icon];
        if (self.entry.goaltime > 0) {
            [self.dateSwitch setOn:YES];
            
            NSDate *d = [NSDate dateWithTimeIntervalSinceReferenceDate:self.entry.goaltime];
            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
            [fmt setDateFormat:@"HH:mm"];
            [fmt setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            self.reminderLabel.text = [NSString stringWithFormat:@"倒计时 - %@", [fmt stringFromDate:d]];
        }
        
        self.title = @"更新任务";
    }
    
    //  notification
    NSNotificationCenter *notifyCenter = [NSNotificationCenter defaultCenter];
    [notifyCenter addObserver:self selector:@selector(textFieldTextDidChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    [notifyCenter addObserver:self selector:@selector(notificationEntriesUpdated:) name:kNotificationEntriesUpdated object:nil];
    
    //  tap gesture
    UITapGestureRecognizer *reg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:reg];
    
    [self checkTitleEmpty];
}

- (void)registerBarButtons
{
    //  left button
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.target = self;
    barButtonItem.action = @selector(cancelAction:);
    barButtonItem.title = @"取消";
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    //  right button
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] init];
    barButtonItem2.title = @"保存";
    barButtonItem2.target = self;
    barButtonItem2.action = @selector(saveAction:);
    self.navigationItem.rightBarButtonItem = barButtonItem2;
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

#pragma mark - Actions

- (void)cancelAction:(id)sender
{
    //  必须要重写它才会响应action
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)saveAction:(id)sender
{
    //  disable the button
    [(UIBarButtonItem *)sender setEnabled:NO];
    
    if (self.entry)
    {
        //  update
        Entry *entry = self.entry;
        entry.title = self.titleField.text;
        entry.remark = self.remarkField.text;
        if ([entry.remark hasSuffix:@"\n"] == NO)
            entry.remark = [NSString stringWithFormat:@"%@\n", entry.remark];
        entry.icon = self.tmpIconName;
        if (self.datePickerVC.datePicker) {
            entry.goaltime = (self.dateSwitch.isOn?self.datePickerVC.datePicker.countDownDuration:0);
        }
        
        [[DataCenter sharedDataCenter] updateEntry:entry];
    }
    else
    {
        Entry *entry = [[Entry alloc] init];
        entry.title = self.titleField.text;
        entry.icon = self.tmpIconName;
        entry.remark = self.remarkField.text;
        if ([entry.remark hasSuffix:@"\n"] == NO)
            entry.remark = [NSString stringWithFormat:@"%@\n", entry.remark];
        entry.timestamp = [NSDate timeIntervalSinceReferenceDate];
        entry.goaltime = (self.dateSwitch.isOn?self.datePickerVC.datePicker.countDownDuration:0);
        
        if (self.plan) {
            entry.plan = self.plan;
        }else{
            entry.plan = [[DataCenter sharedDataCenter] getPlanById:kPlanTableDefault1Id];     // get temp plan
        }
        [[DataCenter sharedDataCenter] newEntry:entry];
    }
}

- (IBAction)iconClicked:(id)sender
{
    //  show the image picker
    [self.titleField resignFirstResponder];
    [self.remarkField resignFirstResponder];
}

- (IBAction)reminderAction:(UISwitch *)sender
{
    //  text field resign both
    [self.titleField resignFirstResponder];
    [self.remarkField resignFirstResponder];
    
    //  logics
    if (sender.isOn)
    {
        [self.datePickerVC showWithSelectionHandler:^(RMDateSelectionViewController *vc, NSDate *aDate) {
            self.reminderLabel.text = [NSString stringWithFormat:@"倒计时 - %@", [self.dateFormatter stringFromDate:aDate]];
            [vc dismiss];
        } andCancelHandler:^(RMDateSelectionViewController *vc) {
            [sender setOn:false animated:YES];
        }];
        
        self.datePickerVC.datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
        self.datePickerVC.datePicker.countDownDuration = 1;
        self.datePickerVC.datePicker.minuteInterval = 1;
    }else {
        self.reminderLabel.text = @"倒计时";
    }
}

#pragma mark - Logic

- (void)checkTitleEmpty
{
    NSString *str = [self.titleField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.navigationItem.leftBarButtonItem.enabled = (str.length <= 0);
    self.navigationItem.rightBarButtonItem.enabled = !(str.length <= 0);
    
    if (self.enableCancel) self.navigationItem.leftBarButtonItem.enabled = YES;
}

- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture
{
    //  resign first responder
    [self.titleField resignFirstResponder];
    [self.remarkField resignFirstResponder];
}

#pragma mark - Notifications

- (void)textFieldTextDidChanged:(NSNotification *)notify
{
    UITextField *textField = (UITextField *)[notify object];
    if (textField == self.titleField) [self checkTitleEmpty];
}

- (void)notificationEntriesUpdated:(NSNotification *)notify
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    void (^animations)() = ^() {
        CGRect frame = self.view.frame;
        frame.origin.y -= 100;
        self.view.frame = frame;
    };
    
    [UIView animateWithDuration:.25 delay:0.0 options:(UIViewAnimationOptionShowHideTransitionViews) animations:animations completion:nil];
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    void (^animations)() = ^() {
        CGRect frame = self.view.frame;
        frame.origin.y += 100;
        self.view.frame = frame;
    };
    
    [UIView animateWithDuration:.25 delay:0.0 options:(UIViewAnimationOptionShowHideTransitionViews) animations:animations completion:nil];
}

#pragma mark - RMImageScrollerDelegate

- (void)imageScroller:(RMImageScroller*)imageScroller centeredImageChanged:(int)index
{
}

- (UIImage*)imageScroller:(RMImageScroller*)imageScroller imageAt:(int)index
{
	return [UIImage imageNamed:[NSString stringWithFormat:@"EA_%04d.png", index+1]];
}

- (int)numberOfImagesInImageScroller:(RMImageScroller*)imageScroller
{
	return 76;
}

- (NSString*)imageScroller:(RMImageScroller*)imageScroller titleForIndex:(int)index
{
    return @"";
}

- (void)imageScroller:(RMImageScroller*)imageScroller selected:(int)index
{
    self.tmpIconName = [NSString stringWithFormat:@"EA_%04d.png", index+1];
    [self.iconView setImage:[UIImage imageNamed:self.tmpIconName]];
    
    [imageScroller setSelectedIndex:index animated:YES];
}

@end
