//
//  MainViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/18/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "EntryListViewController.h"

#import "EntryCell.h"
#import "EntryEditViewController.h"
#import "MKLocalNotificationsScheduler.h"

@implementation EntryListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  notifications
    NSNotificationCenter *notifyCenter = [NSNotificationCenter defaultCenter];
    [notifyCenter addObserver:self selector:@selector(notificationEntriesUpdated:) name:kNotificationEntriesUpdated object:nil];
    
    //  resture the status
    if (self.plan) {
        currentRecord = [[DataCenter sharedDataCenter] getTmpRecord:self.plan.id];
    }
    
    //  register long press on tableview
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0;    //seconds
    [self.tableView addGestureRecognizer:lpgr];
    
    //  加入scheduler
    NSTimer *scheduler = [NSTimer timerWithTimeInterval:0.1f target:self selector:@selector(updateSchedule:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:scheduler forMode:NSRunLoopCommonModes];
    scheduleTimer = scheduler;
    
    self.titlesArr = @[self.title, @"未完成", @"已完成"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //  title gesture
    tapReg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterAction:)];
    tapReg.delegate = self;
    [self.navigationController.navigationBar addGestureRecognizer:tapReg];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar removeGestureRecognizer:tapReg];
}

- (void)willMoveToParentViewController:(UIViewController *)parent
{
    //  为nil则表示pop(测试出来的结果)
    if (parent == nil) {
        [scheduleTimer invalidate];
    }
}

- (void)updateSchedule:(NSTimer *)timr
{
    //  no current record, no need refresh
    if (currentRecord == nil) {
        return;
    }
    
    EntryCell *ec = nil;
    for (EntryCell *cell_ in self.tableView.visibleCells) {
        if (cell_.entry.id == currentRecord.entry.id) { ec = cell_; break; }
    }
    
    //  update current record's entry cell
    if (ec) {
        [ec updateCountDown:currentRecord];
    }
}

- (void)saveRecord:(Record *)rcd
{
    if (rcd && rcd.entry) {
        //  取消之前的notification
        [[MKLocalNotificationsScheduler sharedInstance] cancelNotification:@{@"eTitle": rcd.entry.title}];
    }
    
    if (rcd) {
        //  delete temp at first
        [DBHelper delTmpRecordByPlanId:rcd.entry.plan.id];
        
        //  save condition
        rcd.interval = [NSDate timeIntervalSinceReferenceDate] - rcd.timestamp;
        if (rcd.interval >= kMinSaveInterval)
            [[DataCenter sharedDataCenter] newRecord:rcd];
    }
    
    currentRecord = nil;
}

- (void)startNewRecord:(Entry *)entry
{
    Record *r = [[Record alloc] initWithDate:[NSDate date]];
    r.entry = entry;
    currentRecord = r;
    
    // save tmp record when start new one
    [[DataCenter sharedDataCenter] newTmpRecord:currentRecord planId:entry.plan.id];
    
    // local push notification
    if (entry.goaltime > 0) {
        NSDate *date = [[NSDate date] dateByAddingTimeInterval:entry.goaltime];
        NSString *text = [NSString stringWithFormat:@"「%@」倒计时结束~", entry.title];
        [[MKLocalNotificationsScheduler sharedInstance] scheduleNotificationOn:date text:text action:@"确定" sound:@"alert_blip.caf" launchImage:nil andInfo:@{@"eTitle": entry.title}];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Entry *enty = [self.entries objectAtIndex:indexPath.row];
    
    if (currentRecord.entry.id == enty.id) {
        [self saveRecord:currentRecord];        //  确定是同样的就直接保存
    }else {
        //  detect finished?
        if (enty.finish) {
            NSLog(@"This Entry is already finished !!!");
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            return;
        }
        
        [self saveRecord:currentRecord];
        [self startNewRecord:enty];
    }
    
    //  隐藏所有count down label
    for (EntryCell *cell_ in self.tableView.visibleCells) {
        [cell_ updateCountDown:nil];
    }
    
    //  刷新table的UI
    [tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 52.0f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.entries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"EntityCell";
    EntryCell * cell=  (EntryCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) cell = [[EntryCell alloc] init];
    
    Entry *entry = [self.entries objectAtIndex:indexPath.row];
    cell.eDelegate = self;
    [cell updateEntry:entry];
    
    //  swipe cell configuration
    [cell setDefaultColor:[UIColor lightGrayColor]];
    
    UIImageView *listView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell_list"]];
    listView.contentMode = UIViewContentModeCenter;
    UIColor *yellowColor = [UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0];
    [cell setSwipeGestureWithView:listView color:yellowColor mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        [self loadEntryEditing:indexPath];
    }];
    
    UIImageView *checkView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cell_check"]];
    checkView.contentMode = UIViewContentModeCenter;
    UIColor *greenColor = [UIColor colorWithRed:85.0 / 255.0 green:213.0 / 255.0 blue:80.0 / 255.0 alpha:1.0];
    [cell setSwipeGestureWithView:checkView color:greenColor mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState1 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        [(EntryCell *)cell finishDidClicked:nil];
    }];
    
    return cell;
}

#pragma mark - Logics

- (IBAction)newEntryAction:(id)sender
{
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EntryEditViewController *eevc = [stb instantiateViewControllerWithIdentifier:@"EntryEditViewController"];
    eevc.hidesBottomBarWhenPushed = YES;
    eevc.plan = self.plan;
    [self.navigationController pushViewController:eevc animated:YES];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint p = [gestureRecognizer locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        if (indexPath) {
            confirmEntry = [self.entries objectAtIndex:indexPath.row];
            NSString *title = [NSString stringWithFormat:@"你确定要删除<%@>任务？", confirmEntry.title];
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:title
                                                               delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"删除" otherButtonTitles:nil];
            [sheet showInView:self.view];
        }
    }
}

- (void)loadEntryEditing:(NSIndexPath *)idxPath
{
    if (self.entries.count < 1) {
        return;
    }
    
    Entry *ent = [self.entries objectAtIndex:idxPath.row];
    if (ent) {
        UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EntryEditViewController *eevc = [stb instantiateViewControllerWithIdentifier:@"EntryEditViewController"];
        eevc.hidesBottomBarWhenPushed = YES;
        eevc.entry = ent;
        eevc.enableCancel = YES;
        [self.navigationController pushViewController:eevc animated:YES];
    }
}

- (void)filterAction:(UITapGestureRecognizer *)reg
{
    //  0, 1, 2 - 全部, 完成, 未完成
    long tag = reg.view.tag + 1;
    if (tag > 2) tag = 0;
    reg.view.tag = tag;
    
    self.title = [self.titlesArr objectAtIndex:tag];
    self.entries = [[DataCenter sharedDataCenter] getEntriesByPlanId:self.plan.id type:(int)tag];
    [self.tableView reloadData];
}

#pragma mark - Notifications

- (void)notificationEntriesUpdated:(NSNotification *)notify
{
    self.title = [self.titlesArr objectAtIndex:0];
    self.entries = [[DataCenter sharedDataCenter] getEntriesByPlanId:self.plan.id];
    [self.tableView reloadData];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //  delete
    if (buttonIndex == 0 && confirmEntry) {
        if (confirmEntry.id == currentRecord.entry.id) [self saveRecord:currentRecord];
        
        [[DataCenter sharedDataCenter] deleteEntry:confirmEntry];
        confirmEntry = nil;
        [self.tableView reloadData];
    }
}

#pragma mark - EntryCellDelegate

- (void)entryCellFinishedDidClicked:(EntryCell *)entryCell
{
    if (entryCell.entry.id == currentRecord.entry.id) {
        [self saveRecord:currentRecord];
        [entryCell updateCountDown:nil];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    //  只能通过点击区域来限制
    CGPoint p =[touch locationInView:touch.view];
    return (p.x >= 105.0 && p.x < 210.0);
}

@end
