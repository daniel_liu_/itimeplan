//
//  ListViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/26/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "PlanListViewController.h"

#import "PlanCell.h"
#import "EntryListViewController.h"

#import "SettingViewController.h"
#import "UINavigationController+Fade.h"

#import "SimpleAudioEngine.h"

@implementation PlanListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  data init
    plans = [DataCenter sharedDataCenter].plans;
    
    self.title = @"计划列表";
    
    //  table view
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //  register long press on tableview
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0;    //seconds
    [self.tableView addGestureRecognizer:lpgr];
    
    //  notification
    NSNotificationCenter *notifyCenter = [NSNotificationCenter defaultCenter];
    [notifyCenter addObserver:self selector:@selector(notificationPlansUpdated:) name:kNotificationPlansUpdated object:nil];
    [notifyCenter addObserver:self selector:@selector(notificationReceiveLocalNotification:) name:@"NotificationLocalNotify" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Plan *plan = [plans objectAtIndex:indexPath.row];
    
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EntryListViewController *elv = [stb instantiateViewControllerWithIdentifier:@"EntryListViewController"];
    elv.title = plan.title;
    elv.plan = plan;
    elv.entries = [[DataCenter sharedDataCenter] getEntriesByPlanId:plan.id];
    [self.navigationController pushViewController:elv animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72.0f;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return plans.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PlanCell";
    
    PlanCell * cell=  (PlanCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) cell = [[PlanCell alloc] init];
    
    Plan *p = [plans objectAtIndex:indexPath.row];
    cell.iconView.image = [UIImage imageNamed:p.icon];
    cell.titleLabel.text = p.title;
    cell.remarkLabel.text = (p.remark==nil||[p.remark length]<1)?@"无描述":p.remark;
    
    //  badge for not finished entries
    int b = [[DataCenter sharedDataCenter] getTodoBadgeInPlan:p.id];
    if (b > 0) {
        cell.badge.fontSize = 12;
        cell.badgeLeftOffset = 8;
        cell.badgeRightOffset = 40;
        cell.badgeString = [NSString stringWithFormat:@"%d", b];
        
        Record *r = [[DataCenter sharedDataCenter] getTmpRecord:p.id];
        if (r) cell.badgeColor = [UIColor colorWithRed:0.197 green:0.792 blue:0.219 alpha:1.000];       // running is green
        else cell.badgeColor = [UIColor colorWithRed:0.792 green:0.197 blue:0.219 alpha:1.000];         // normal is red
    }
    cell.badge.alpha = (b>0?1:0);
    
    return cell;
}

#pragma mark - Logic

- (IBAction)settingClicked:(id)sender
{
    UIStoryboard *stb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SettingViewController *svc = [stb instantiateViewControllerWithIdentifier:@"SettingViewController"];
    [self.navigationController pushViewControllerFade:svc];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint p = [gestureRecognizer locationInView:self.tableView];
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        if (indexPath)
        {
            confirmPlan = [plans objectAtIndex:indexPath.row];
            if (confirmPlan.id == kPlanTableTempId) {
                confirmPlan = nil;
                return;
            }
            UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"删除计划将会同时删除子任务(不可恢复)，你确定吗？"
                                                               delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"删除" otherButtonTitles:nil];
            [sheet showInView:self.view];
        }
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //  delete
    if (buttonIndex == 0 && confirmPlan) {
        [[DataCenter sharedDataCenter] deletePlan:confirmPlan];
        [[DataCenter sharedDataCenter] destoryEntriesByPlanId:confirmPlan.id];
        confirmPlan = nil;
    }
}

#pragma mark - Notifications

- (void)notificationPlansUpdated:(NSNotification *)notify
{
    plans = [DataCenter sharedDataCenter].plans;
    [self.tableView reloadData];
}

- (void)notificationReceiveLocalNotification:(NSNotification *)notify
{
    NSLog(@"Application did receive local notification: %@ in PlanListViewController(RootViewController)", notify);
    
    if (notify.userInfo && [notify.userInfo objectForKey:@"eTitle"])
    {
        //  play sound effect
        NSString *path = [[NSBundle mainBundle] pathForResource:@"alert_xylophone.caf" ofType:nil];
        [[SimpleAudioEngine sharedEngine] playEffect:path];
        
        //  show alert view
        NSString *entryTitle = [notify.userInfo objectForKey:@"eTitle"];
        NSString *msg = [NSString stringWithFormat:@"「%@」倒计时结束~", entryTitle];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alert show];
    }
}

@end

