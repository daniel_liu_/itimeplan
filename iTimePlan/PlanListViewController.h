//
//  ListViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/26/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataCenter.h"

@interface PlanListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
{
    NSMutableArray *plans;
    Plan *confirmPlan;          // sure to delete?
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)settingClicked:(id)sender;
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;

- (void)notificationPlansUpdated:(NSNotification *)notify;
- (void)notificationReceiveLocalNotification:(NSNotification *)notify;

@end

