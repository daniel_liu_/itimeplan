//
//  UINavigationController+Fade.h
//  iTimePlan
//
//  Created by Ryukanmei on 3/31/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UINavigationController (Fade)

- (void)pushViewControllerFade:(UIViewController *)viewController;
- (void)popViewControllerFade;

@end
