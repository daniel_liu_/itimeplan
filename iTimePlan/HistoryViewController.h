//
//  HistoryViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/18/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DataCenter.h"
#import "Record.h"
#import "MZDayPicker.h"
#import "ACNavBarDrawer.h"

@interface HistoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MZDayPickerDelegate, MZDayPickerDataSource, ACNavBarDrawerDelegate>
{
    NSArray *recordsArray;
    Record *currentRecord;
    
    ACNavBarDrawer *navBarDrawer;
    Plan *currentPlan;
    UITapGestureRecognizer *tapReg;
}

@property (strong, nonatomic) NSArray *plans;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *bannerView;

@property (nonatomic, strong) MZDayPicker *dayPicker;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

- (IBAction)chartAction:(id *)sender;
- (void)navigationBarTapped:(id)sender;

- (void)notificationRecordsUpdated:(NSNotification *)notify;

@end

