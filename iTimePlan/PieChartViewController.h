//
//  PieChartViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/20/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Plan.h"
#import "XYPieChart.h"
#import "MZDayPicker.h"
#import "ACNavBarDrawer.h"

@interface PieChartViewController : UIViewController <XYPieChartDelegate, XYPieChartDataSource, MZDayPickerDataSource, MZDayPickerDelegate, ACNavBarDrawerDelegate>
{
    NSDictionary *recordsDict;
    
    ACNavBarDrawer *navBarDrawer;
    Plan *currentPlan;
    UITapGestureRecognizer *tapReg;
}

@property (strong, nonatomic) NSArray *plans;

@property (weak, nonatomic) IBOutlet XYPieChart *pieChart;
@property (weak, nonatomic) IBOutlet UIView *bannerView;

@property (strong, nonatomic) MZDayPicker *dayPicker;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

@property (weak, nonatomic) IBOutlet UIImageView *bottomIconView;
@property (weak, nonatomic) IBOutlet UILabel *bottomTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalHoursLabel;

- (void)pieChartClickedByKey:(NSString *)key;
- (void)historyAction:(id)sender;
- (void)filterAction:(id)sender;

@end
