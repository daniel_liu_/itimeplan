//
//  PieChartViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/20/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "PieChartViewController.h"

#import "NSDate+DateString.h"
#import "DataCenter.h"
#import "ColorHelper.h"

#import "UINavigationController+Fade.h"

#define kTotalIconName  @"EA_0039.png"
#define kTotalTitle     @"当日总汇"

@implementation PieChartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  pie chart
    [self.pieChart setDataSource:self];
    [self.pieChart setDelegate:self];
    [self.pieChart setStartPieAngle:M_PI_2];
    [self.pieChart setAnimationSpeed:1.0];
    [self.pieChart setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:22]];
    [self.pieChart setLabelRadius:80];
    [self.pieChart setShowPercentage:YES];
    [self.pieChart setBackgroundColor:[UIColor clearColor]];
    [self.pieChart setPieCenter:CGPointMake(120, 120)];
    [self.pieChart setUserInteractionEnabled:YES];
    [self.pieChart setLabelShadowColor:[UIColor blackColor]];
    
    //  day picker
    self.dayPicker = [[MZDayPicker alloc] initWithFrame:CGRectMake(0, 0, 320, 52) dayCellSize:CGSizeZero dayCellFooterHeight:0.0f];
    self.dayPicker.delegate = self;
    self.dayPicker.dataSource = self;
    self.dayPicker.dayNameLabelFontSize = 10.0f;
    self.dayPicker.dayLabelFontSize = 16.0f;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"EE"];
    NSDate *day = [NSDate date];
    NSDate *last7 = [day dateByAddingTimeInterval:-7*(24*60*60)];   // 7天内
    [self.dayPicker setStartDate:last7 endDate:day];
    [self.dayPicker setCurrentDate:day animated:NO];
    [self.bannerView addSubview:self.dayPicker];
    
    //  nav bar
    self.plans = [[DataCenter sharedDataCenter] getLastModifyPlans:kLastModifyPlanNum];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [items addObject:@[kTotalIconName, kTotalTitle]];
    for (Plan *p in self.plans) [items addObject:@[p.icon, p.title]];
    navBarDrawer = [[ACNavBarDrawer alloc] initWithView:self.view andItemInfoArray:items];
    navBarDrawer.delegate = self;
    
    //  UI
    currentPlan = nil;
    self.title = kTotalTitle;
    [self registerBarButtons];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (currentPlan == nil) {
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDay:[NSDate date]];
    }else {
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDate:[NSDate date] planId:currentPlan.id];
    }
    [self.pieChart reloadData];
    
    //  title gesture
    tapReg = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(filterAction:)];
    [self.navigationController.navigationBar addGestureRecognizer:tapReg];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar removeGestureRecognizer:tapReg];
}

- (void)registerBarButtons
{
    //  left button
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.target = self;
    barButtonItem.action = @selector(historyAction:);
    barButtonItem.title = @"记录";
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

#pragma mark - MZDayPickerDataSource

- (NSString *)dayPicker:(MZDayPicker *)dayPicker titleForCellDayNameLabelInDay:(MZDay *)day
{
    return [self.dateFormatter stringFromDate:day.date];
}

#pragma mark - MZDayPickerDelegate

- (void)dayPicker:(MZDayPicker *)dayPicker didSelectDay:(MZDay *)day
{
    if (currentPlan == nil) {
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDay:day.date];
    }else {
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDate:day.date planId:currentPlan.id];
    }
    [self.pieChart reloadData];
}

#pragma mark - XYPieChart Data Source

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return [recordsDict allKeys].count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    NSArray *keys = [recordsDict allKeys];
    NSString *key = [keys objectAtIndex:index];
    NSArray *records = [recordsDict objectForKey:key];
    
    NSTimeInterval total = 0.0f;
    for (Record *rcd in records) total += rcd.interval;
    return total;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return [ColorHelper randomColor];
}

#pragma mark - XYPieChart Delegate

- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
{
}

- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
{
}

- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
}

- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    NSArray *keys = [recordsDict allKeys];
    NSString *key = [keys objectAtIndex:index];
    [self pieChartClickedByKey:key];
}

#pragma mark - ACNavBarDrawerDelegate

- (void)didTapButtonAtIndex:(NSInteger)itemIndex
{
    //  0 = 今日总汇
    if (itemIndex == 0)
    {
        self.title = kTotalTitle;
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDay:self.dayPicker.currentDate];
    }
    else
    {
        currentPlan = [self.plans objectAtIndex:itemIndex-1];   // minus 1
        
        self.title = currentPlan.title;
        recordsDict = [[DataCenter sharedDataCenter] getRecordsTitleDictByDate:self.dayPicker.currentDate planId:currentPlan.id];
    }
    [self.pieChart reloadData];
}

#pragma mark - Actions

- (void)pieChartClickedByKey:(NSString *)key
{
    NSArray *records = [recordsDict objectForKey:key];
    NSTimeInterval total = 0.0f;
    for (Record *rcd in records) total += rcd.interval;
    
    self.totalHoursLabel.text = [Utility stringByInterval:total short:NO];
    self.bottomTitleLabel.text = key;
    
    Record *r_ = [records lastObject];
    UIImage *img_ = [UIImage imageNamed:r_.entry.icon];
    self.bottomIconView.image = img_;
}

- (void)historyAction:(id)sender
{
    [self.navigationController popViewControllerFade];
}

- (void)filterAction:(id)sender
{
    if (navBarDrawer.isOpen) [navBarDrawer closeNavBarDrawer];
    else [navBarDrawer openNavBarDrawer];
}

@end

