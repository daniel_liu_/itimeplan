//
//  SettingViewController.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/31/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "SettingViewController.h"
#import "UINavigationController+Fade.h"

#import "DataCenter.h"

@implementation SettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"设置";
    
    //  left button
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] init];
    barButtonItem.target = self;
    barButtonItem.action = @selector(doneClicked:);
    barButtonItem.title = @"关闭";
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

- (void)doneClicked:(id)sender
{
    [self.navigationController popViewControllerFade];
}

- (IBAction)exportSample1:(id)sender
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fullPath = [path stringByAppendingPathComponent:@"sample1.json"];
    [[DataCenter sharedDataCenter] exportPlan:1 toPath:fullPath];
}

- (IBAction)importSample1:(id)sender
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fullPath = [path stringByAppendingPathComponent:@"sample1.json"];
    [[DataCenter sharedDataCenter] importPlanFromPath:fullPath];
}

@end
