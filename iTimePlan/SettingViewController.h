//
//  SettingViewController.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/31/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController

- (void)doneClicked:(id)sender;

- (IBAction)exportSample1:(id)sender;
- (IBAction)importSample1:(id)sender;

@end
