//
//  Utility.m
//  iTimePlan
//
//  Created by LiuHuanMing on 5/5/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+ (NSString *)stringByInterval:(float)intv short:(BOOL)flag
{
    float hours = intv / 3600;
    float mins = intv / 60;
    
    if (flag)
    {
        if (hours < 1) return (mins < 1)?@"":[NSString stringWithFormat:@"%.0fm", mins];
        return [NSString stringWithFormat:@"%0.1fh", hours];
    }
    else
    {
        if (mins < 1) return @"<1m";
        int hour = (int)hours;
        int min = (int)mins-((int)hours*60);
        return [NSString stringWithFormat:@"%@%@",
                hour>0?[NSString stringWithFormat:@"%dh", hour]:@"",
                min>0?[NSString stringWithFormat:@"%dm", min]:@""];
    }
}

+ (NSString *)stringByInterval:(float)intv
{
    return [Utility stringByInterval:intv short:YES];
}

@end
