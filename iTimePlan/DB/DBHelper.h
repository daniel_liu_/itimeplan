//
//  DBHelper.h
//  iTimePlan
//
//  Created by Ryukanmei on 3/20/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CJSONDeserializer.h"
#import "CJSONSerializer.h"

#import "FMDatabase.h"
#import "NSDate+DateString.h"

#define kPlanTableTempId        1           // 计划表-临时计划表id
#define kPlanTableDefault1Id    2           // 计划表-默认1表id

@interface DBHelper : NSObject

//  plan
+ (NSArray *)getAllPlans;
+ (NSArray *)getLastModifyPlans:(int)num;   // 获取最近更新过的plans
+ (NSDictionary *)getPlanById:(int)planid;
+ (int)newPlan:(NSDictionary *)plan;
+ (void)updatePlan:(NSDictionary *)plan;
+ (void)deletePlan:(int)pid;
+ (int)getTodoBadgeInPlan:(int)pid;         // 获取未完成的badge数

//  entry
+ (NSArray *)getAllEntries;
+ (NSArray *)getEntriesByPlanId:(int)planid;
+ (int)newEntry:(NSDictionary *)entry;
+ (void)updateEntry:(NSDictionary *)entry;
+ (void)deleteEntry:(int)eid;
+ (BOOL)destoryEntriesByPlanId:(int)planid;
+ (NSTimeInterval)getEntrySeconds:(int)eid;

//  tmp record
+ (int)newTmpRecord:(NSDictionary *)record;
+ (NSDictionary *)getTmpRecord:(int)planId;
+ (void)delTmpRecordByPlanId:(int)playId;

//  record
+ (int)newRecord:(NSDictionary *)record;
+ (void)updateRecord:(NSDictionary *)record;        // 这个方法不需要，时间是由record组成的，不能改子元素
+ (NSArray *)getAllRecords;
+ (NSArray *)getRecordsByDate:(NSDate *)day planId:(int)planId;
+ (NSArray *)getRecordsByDate:(NSDate *)day;
+ (NSArray *)getRecordsByDay:(NSDate *)day;

//  db
+ (BOOL)hasDBFile;
+ (void)initDBFile;
+ (NSString *)getDBFullPath;

@end
