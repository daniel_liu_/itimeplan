//
//  Plan.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/28/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Plan : NSObject
{
    int id;
    NSString *title;
    NSString *icon;
    NSString *remark;
    NSTimeInterval timestamp;
}

@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, assign) NSTimeInterval timestamp;

@end
