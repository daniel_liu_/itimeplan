//
//  Plan.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/28/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "Plan.h"

@implementation Plan

@synthesize id;
@synthesize title;
@synthesize icon;
@synthesize remark;
@synthesize timestamp;

- (NSString *)description
{
    return [NSString stringWithFormat:@"[Plan: %d, %@]", id, title];
}

@end
