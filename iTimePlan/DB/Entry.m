//
//  Entry.m
//  iTimePlan
//
//  Created by Ryukanmei on 3/21/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "Entry.h"

@implementation Entry

@synthesize id;
@synthesize title;
@synthesize icon;
@synthesize remark;
@synthesize timestamp;
@synthesize goaltime;
@synthesize finish;

@synthesize plan;

- (NSString *)description
{
    return [NSString stringWithFormat:@"[Entry: %d, %@]", id, title];
}

@end
