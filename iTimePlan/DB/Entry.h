//
//  Entry.h
//  iTimePlan
//
//  Created by Ryukanmei on 3/21/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Plan.h"

@interface Entry : NSObject
{
    int id;
    NSString *title;
    NSString *icon;
    NSString *remark;
    NSTimeInterval timestamp;
    NSTimeInterval goaltime;
    int finish;
    
    Plan *plan;
}

@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *remark;
@property (nonatomic, assign) NSTimeInterval timestamp;
@property (nonatomic, assign) NSTimeInterval goaltime;
@property (nonatomic, assign) int finish;

@property (nonatomic, strong) Plan *plan;

@end
