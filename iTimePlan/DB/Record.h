//
//  Record.h
//  iTimePlan
//
//  Created by LiuHuanMing on 3/21/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSDate+DateString.h"
#import "Entry.h"

@interface Record : NSObject
{
    int id;
    NSString *date;
    NSString *month;
    NSString *week;
    NSTimeInterval timestamp;
    NSTimeInterval interval;
    
    Entry *entry;
}

@property (nonatomic, assign) int id;
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *month;
@property (nonatomic, strong) NSString *week;
@property (nonatomic, assign) NSTimeInterval timestamp;
@property (nonatomic, assign) NSTimeInterval interval;

@property (nonatomic, strong) Entry *entry;

- (id)initWithDate:(NSDate *)aDate;

- (NSString *)durationStr;
- (NSString *)hoursStr;

@end
