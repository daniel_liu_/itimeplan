//
//  DBHelper.m
//  iTimePlan
//
//  Created by Ryukanmei on 3/20/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "DBHelper.h"

#define kDBFilePath     @"iTimePlan.sqlite"

@implementation DBHelper

#pragma mark - Plan

+ (NSArray *)getAllPlans
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from plan"];
    while ([rs next]) {
        [tmpArray addObject:[rs resultDictionary]];
    }
    [rs close];
    [db close];
    
    return tmpArray;
}

// 获取最近更新过的plans
+ (NSArray *)getLastModifyPlans:(int)num
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    //  1. get last modified records
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    
    FMResultSet *rs = [db executeQuery:@"select entryid from record order by id desc limit 200"];     // 为了性能更好，只在200条数据内
    NSMutableArray *entryids = [[NSMutableArray alloc] init];
    while ([rs next]) [entryids addObject:[NSNumber numberWithInt:[rs intForColumn:@"entryid"]]];
    [rs close];
    
    NSString *mgStr = [entryids componentsJoinedByString:@","];
    NSString *stmt = [NSString stringWithFormat:@"select DISTINCT planid from entry where id in (%@)", mgStr];
    FMResultSet *rs2 = [db executeQuery:stmt];
    NSMutableArray *planids = [[NSMutableArray alloc] init];
    int idx = 0;
    while ([rs2 next]) {
        [planids addObject:[NSNumber numberWithInt:[rs2 intForColumn:@"planid"]]];
        if (++idx >= num) break;
    }
    [rs2 close];
    
    NSString *mgStr2 = [planids componentsJoinedByString:@","];
    NSString *stmt2 = [NSString stringWithFormat:@"select * from plan where id in (%@)", mgStr2];
    FMResultSet *rs3 = [db executeQuery:stmt2];
    while ([rs3 next]) [tmpArray addObject:[rs3 resultDictionary]];
    [rs3 close];
    
    [db close];
    
    //  2. no last modified?
    if (tmpArray.count < 1) {
        NSArray *arr = [self getAllPlans];
        int maxl = (arr.count>num)?num:arr.count;
        for (int i = 0; i < maxl; i++) {
            [tmpArray addObject:[arr objectAtIndex:i]];
        }
    }
    
    return tmpArray;
}

+ (NSDictionary *)getPlanById:(int)planid
{
    NSDictionary *tmpDict = nil;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from plan where id = ?", [NSNumber numberWithInt:planid]];
    while ([rs next]) {
        tmpDict = [[rs resultDictionary] mutableCopy];
    }
    [rs close];
    [db close];
    return tmpDict;
}

+ (int)newPlan:(NSDictionary *)plan
{
    int nid = 0;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"insert into plan(title, icon, remark, timestamp) values(:title, :icon, :remark, :timestamp)" withParameterDictionary:plan];
    nid = (int)[db lastInsertRowId];
    [db close];
    return nid;
}

+ (void)updatePlan:(NSDictionary *)plan
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"update plan set title=?, icon=?, remark=?, timestamp=? where id=?",
     [plan objectForKey:@"title"], [plan objectForKey:@"icon"], [plan objectForKey:@"remark"], [plan objectForKey:@"timestamp"], [plan objectForKey:@"id"]];
    [db close];
}

/*
    TODO::
    这里并没有处理冗余的信息，当初考虑的是自动将子任务移自"临时列表"，
    这里最好能判断子任务的个数，如果小于8，则提示会移动到"临时列表"，
    但如果大于这个值，则需要用户自己先去处理才能删除(让用户三思而后行)
    --
    上层处理
 */
+ (void)deletePlan:(int)pid
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"delete from plan where id = ?", [NSNumber numberWithInteger:pid]];
    [db close];
}

// 获取未完成的badge数
+ (int)getTodoBadgeInPlan:(int)pid
{
    int num = 0;
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select count(*) from entry where planid = ? AND finish = 0", [NSNumber numberWithInt:pid]];
    while ([rs next]) {
        num = [rs intForColumnIndex:0];
    }
    [rs close];
    [db close];
    
    return num;
}

#pragma mark - entry

+ (NSArray *)getAllEntries
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from entry"];
    while ([rs next]) {
        [tmpArray addObject:[rs resultDictionary]];
    }
    [rs close];
    [db close];
    
    return tmpArray;
}

+ (NSArray *)getEntriesByPlanId:(int)planid
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from entry where planid = ?", [NSNumber numberWithInt:planid]];
    while ([rs next]) {
        [tmpArray addObject:[rs resultDictionary]];
    }
    [rs close];
    [db close];
    
    return tmpArray;
}

+ (int)newEntry:(NSDictionary *)entry
{
    int nid = 0;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"insert into entry(title, icon, remark, timestamp, goaltime, planid) values(:title, :icon, :remark, :timestamp, :goaltime, :planid)" withParameterDictionary:entry];
    nid = (int)[db lastInsertRowId];
    [db close];
    return nid;
}

+ (void)updateEntry:(NSDictionary *)entry
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"update entry set title=?, icon=?, remark=?, timestamp=?, goaltime=?, planid=?, finish=? where id=?",
     [entry objectForKey:@"title"], [entry objectForKey:@"icon"], [entry objectForKey:@"remark"], [entry objectForKey:@"timestamp"], [entry objectForKey:@"goaltime"], [entry objectForKey:@"planid"], [entry objectForKey:@"finish"], [entry objectForKey:@"id"]];
    [db close];
}

+ (void)deleteEntry:(int)eid
{
    NSNumber *n = [NSNumber numberWithInteger:eid];
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"delete from entry where id = ?", n];
    [db executeUpdate:@"delete from record where entryid = ?", n];
    [db executeUpdate:@"delete from record_tmp where entryid = ?", n];
    [db close];
}

+ (BOOL)destoryEntriesByPlanId:(int)planid
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    
    FMResultSet *rs1 = [db executeQuery:@"select id from entry where planid = ?", [NSNumber numberWithInt:planid]];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    while ([rs1 next]) [arr addObject:[NSNumber numberWithInt:[rs1 intForColumn:@"id"]]];
    [rs1 close];
    
    NSString *mergeIdString = [arr componentsJoinedByString:@","];
    NSString *st1 = [NSString stringWithFormat:@"delete from entry where id in (%@)", mergeIdString];
    NSString *st2 = [NSString stringWithFormat:@"delete from record where entryid in (%@)", mergeIdString];
    NSString *st3 = [NSString stringWithFormat:@"delete from record_tmp where entryid in (%@)", mergeIdString];
    BOOL a = [db executeUpdate:st1];
    BOOL b = [db executeUpdate:st2];
    BOOL c = [db executeUpdate:st3];
    
    [db close];
    
    return a&&b&&c;
}

+ (NSTimeInterval)getEntrySeconds:(int)eid
{
    double total = 0;
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select sum(interval) from record where entryid = ?", [NSNumber numberWithInt:eid]];
    while ([rs next]) {
        total += [rs doubleForColumnIndex:0];
    }
    [rs close];
    [db close];
    
    return total;
}

#pragma mark - tmp record

+ (int)newTmpRecord:(NSDictionary *)record
{
    int nid = 0;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"insert into record_tmp(date, month, week, timestamp, interval, entryid, planid) values(:date, :month, :week, :timestamp, :interval, :entryid, :planid)" withParameterDictionary:record];
    nid = (int)[db lastInsertRowId];
    [db close];
    return nid;
}

+ (NSDictionary *)getTmpRecord:(int)planId
{
    NSDictionary *tmpRecord = nil;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from record_tmp where planid = ?", [NSNumber numberWithInt:planId]];
    while ([rs next]) {
        tmpRecord = [rs resultDictionary];
        break;  // only for the 1st data
    }
    [rs close];
    [db close];
    return tmpRecord;
}

+ (void)delTmpRecordByPlanId:(int)playId
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"delete from record_tmp where planid = ?", [NSNumber numberWithInt:playId]];
    [db close];
}

#pragma mark - record

+ (int)newRecord:(NSDictionary *)record
{
    int nid = 0;
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
//    [db executeUpdate:@"delete from record_tmp where planid = ?", [record objectForKey:@"planid"]];
    [db executeUpdate:@"insert into record(date, month, week, timestamp, interval, entryid) values(:date, :month, :week, :timestamp, :interval, :entryid)" withParameterDictionary:record];
    nid = (int)[db lastInsertRowId];
    [db close];
    return nid;
}

+ (void)updateRecord:(NSDictionary *)record
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    [db executeUpdate:@"update record set date=?, month=?, week=?, timestamp=?, interval=?,entryid=? where id=?",
     [record objectForKey:@"date"], [record objectForKey:@"month"], [record objectForKey:@"week"], [record objectForKey:@"timestamp"], [record objectForKey:@"interval"], [record objectForKey:@"entryid"], [record objectForKey:@"id"]];
    [db close];
}

+ (NSArray *)getAllRecords
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from record"];
    while ([rs next]) {
        [tmpArray addObject:[rs resultDictionary]];
    }
    [rs close];
    [db close];
    
    return tmpArray;
}

/*
 day string looks like "20130318"
 */
+ (NSArray *)getRecordByDateStr:(NSString *)dayStr planId:(int)planId
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    
    if (planId <=0) planId = kPlanTableTempId;     // deafult planid is temp plan
    
    FMResultSet *rs = [db executeQuery:@"select id from entry where planid = ?", [NSNumber numberWithInt:planId]];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    while ([rs next]) [arr addObject:[NSNumber numberWithInt:[rs intForColumn:@"id"]]];
    [rs close];

    NSString *mergeIdString = [arr componentsJoinedByString:@","];
    NSString *stmt = [NSString stringWithFormat:@"select * from record where date=%@ AND entryid IN (%@)", dayStr, mergeIdString];
    FMResultSet *rs2 = [db executeQuery:stmt];
    while ([rs2 next]) [tmpArray addObject:[rs2 resultDictionary]];
    [rs2 close];
    
    [db close];
    
    return tmpArray;
}

/*
 区别在于不会默认取 temp table
 */
+ (NSArray *)getRecordsByDay:(NSDate *)day
{
    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    FMResultSet *rs = [db executeQuery:@"select * from record where date=?", [day dateString]];
    while ([rs next]) [tmpArray addObject:[rs resultDictionary]];
    [db close];
    
    return tmpArray;
}

+ (NSArray *)getRecordsByDate:(NSDate *)day planId:(int)planId
{
    return [[self class] getRecordByDateStr:[day dateString] planId:planId];
}

+ (NSArray *)getRecordsByDate:(NSDate *)day
{
    return [[self class] getRecordByDateStr:[day dateString] planId:kPlanTableTempId];
}

#pragma mark - db

+ (BOOL)hasDBFile
{
    NSString *fullPath = [[self class] getDBFullPath];
    return [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
}

+ (void)initDBFile
{
    FMDatabase *db = [FMDatabase databaseWithPath:[[self class] getDBFullPath]];
    [db open];
    
    //  create plan table
    [db executeUpdate:@"create table plan(id integer primary key autoincrement, title text, icon text, remark text, timestamp double)"];
    
    //  create entry table
    [db executeUpdate:@"create table entry(id integer primary key autoincrement, title text, icon text, remark text, timestamp double, goaltime double default(0), finish integer default(0), planid integer)"];
    
    //  create record table
    [db executeUpdate:@"create table record(id integer primary key autoincrement, date text, month text, week text, timestamp double, interval double, entryid int)"];
    
    //  create temp record table
    [db executeUpdate:@"create table record_tmp(id integer primary key autoincrement, date text, month text, week text, timestamp double, interval double, entryid int, planid int)"];
    
    [db close];
}

+ (NSString *)getDBFullPath
{
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *fullPath = [docPath stringByAppendingPathComponent:kDBFilePath];
    return fullPath;
}

@end
