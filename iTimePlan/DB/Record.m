//
//  Record.m
//  iTimePlan
//
//  Created by LiuHuanMing on 3/21/14.
//  Copyright (c) 2014 Daniel.Ryu. All rights reserved.
//

#import "Record.h"

@implementation Record

@synthesize id;
@synthesize date;
@synthesize month;
@synthesize week;
@synthesize timestamp;
@synthesize interval;

@synthesize entry;

- (NSString *)description
{
    return [NSString stringWithFormat:@"[Record: %d, %@, %f, %f]",
            id, [[NSDate dateWithTimeIntervalSinceReferenceDate:timestamp] timeString], timestamp, interval];
}

- (id)initWithDate:(NSDate *)aDate
{
    self = [super init];
    if (self) {
        self.timestamp = [aDate timeIntervalSinceReferenceDate];
        self.date = [aDate dateString];
        self.month = [aDate monthString];
        self.week = [aDate weekString];
    }
    return self;
}

- (NSString *)durationStr
{
    NSDate *day = [NSDate dateWithTimeIntervalSinceReferenceDate:timestamp];
    NSDate *day2 = [day dateByAddingTimeInterval:self.interval];
    return [NSString stringWithFormat:@"%@ - %@", [day shorttimeString], [day2 shorttimeString]];
}

- (NSString *)hoursStr
{
    return [Utility stringByInterval:interval];
}

@end
