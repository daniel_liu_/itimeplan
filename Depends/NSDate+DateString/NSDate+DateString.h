//
//  NSDate+DateString.h
//  BirdDemo
//
//  Created by Ryukanmei on 3/18/14.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (DateString)

/* 20140317 */
- (NSString *)dateString;

/* 2014.03.17 */
- (NSString *)formDateString;

/* 201403 */
- (NSString *)monthString;

/* 201412 */
- (NSString *)weekString;

/* 13:12:33 */
- (NSString *)timeString;

/* 12:15 */
- (NSString *)shorttimeString;

@end

