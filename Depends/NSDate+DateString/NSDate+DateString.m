//
//  NSDate+DateString.m
//  BirdDemo
//
//  Created by Ryukanmei on 3/18/14.
//
//

#import "NSDate+DateString.h"

@implementation NSDate (DateString)

- (NSString *)dateString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyyMMd"];
    return [fmt stringFromDate:self];
}

- (NSString *)formDateString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy.MM.d"];
    return [fmt stringFromDate:self];
}

- (NSString *)monthString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyyMM"];
    return [fmt stringFromDate:self];
}

- (NSString *)weekString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyyw"];
    return [fmt stringFromDate:self];
}

- (NSString *)timeString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"HH:mm:ss"];
    return [fmt stringFromDate:self];
}

- (NSString *)shorttimeString
{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"HH:mm"];
    return [fmt stringFromDate:self];
}

@end

