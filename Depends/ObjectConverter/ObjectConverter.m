//
//  ObjectConverter.m
//  AppLogicDemo
//
//  Created by Ming Daniel on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ObjectConverter.h"
#import <objc/runtime.h>

@implementation ObjectConverter

static NSMutableArray *names = nil;

+ (void)createNamesForClass:(Class)theClass {
    
    if (names == nil) {
        names = [NSMutableArray arrayWithCapacity:10];
    }
    
    unsigned int varCount;
    Ivar *vars = class_copyIvarList(theClass, &varCount);
    for (int i = 0; i < varCount; i++) {
        Ivar var = vars[i];
        NSString *name = [[NSString alloc] initWithUTF8String:ivar_getName(var)];
        if (ivar_getTypeEncoding(var)[0] == _C_STRUCT_B || ivar_getTypeEncoding(var)[0] == _C_STRUCT_E) {
//          NSLog(@"struct type property: %@ [ignored]", name);
            continue;
        }
        [names addObject:name];
    }
    free(vars);
    
    if ([theClass superclass] != [NSObject class]) {
        [self createNamesForClass:[theClass superclass]];
    }
}

+ (id)createObject:(Class)aClass fromDictionary:(NSDictionary *)dict {
    
    //  create names
    [names removeAllObjects];
    [[self class] createNamesForClass:aClass];
    
    //  set values
    id object = [[aClass alloc] init];
    for (NSString* name in names) {
        id obj = [dict objectForKey:name];
        if (obj) [object setValue:obj forKey:name];
    }
    return object;
}

+ (NSArray *)createObjects:(Class)aClass fromArray:(NSArray *)array {
    
    //  create names
    [names removeAllObjects];
    [[self class] createNamesForClass:aClass];
    
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:array.count];
    for (NSDictionary *dict in array) {
        //  set values
        id object = [[aClass alloc] init];
        for (NSString* name in names) {
            id obj = [dict objectForKey:name];
            if (obj) [object setValue:obj forKey:name];
        }
        [result addObject:object];
    }
    return result;
}

+ (NSMutableDictionary *)createDictionaryFromObject:(id)obj
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    unsigned count;
    objc_property_t *properties = class_copyPropertyList([obj class], &count);
    
    for (int i = 0; i < count; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        id o = [obj valueForKey:key];
        if (o) [dict setObject:o forKey:key];
    }
    free(properties);
    
    return dict;
}

@end
