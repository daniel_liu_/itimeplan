//
//  ObjectConverter.h
//  AppLogicDemo
//
//  Created by Ming Daniel on 3/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Version: 1.2.0

#import <Foundation/Foundation.h>

@interface ObjectConverter : NSObject

+ (id)createObject:(Class)aClass fromDictionary:(NSDictionary *)dict;
+ (NSArray *)createObjects:(Class)aClass fromArray:(NSArray *)array;
+ (NSMutableDictionary *)createDictionaryFromObject:(id)obj;

@end
